extends Area2D

onready var number = get_parent().number

func _on_itemarea_body_entered(body):
	if body.get_name() == "Player" or body.get_name() == "Player2":
		if number < 1:
			global.score += 5
			get_parent().get_parent().get_node("Positive").play()
		elif number < 2:
			global.score += 10
			get_parent().get_parent().get_node("Positive").play()
		elif number < 3:
			global.score += 20
			get_parent().get_parent().get_node("Positive").play()
		elif number < 4:
			if global.lives < 5:
				global.lives += 1
			get_parent().get_parent().get_node("Positive").play()
		elif (number < 5):
			if global.lives <= 2:
				global.lives -=2
				get_tree().change_scene(str("res://Scenes/gameOver.tscn"))
			else:
				global.lives -= 2
				get_parent().get_parent().get_node("Negative").play()
		elif (number < 7):
			if global.lives == 1:
				global.lives -= 1
				get_tree().change_scene(str("res://Scenes/gameOver.tscn"))
			else:
				global.lives -= 1
				get_parent().get_parent().get_node("Negative").play()
		elif (number < 7.5):
			if global.detik < 57:
				global.detik += 3
				get_parent().get_parent().get_node("Positive").play()
			else:
				global.detik = 60
				get_parent().get_parent().get_node("Positive").play()
		elif (number < 7.75):
			if global.detik < 55:
				global.detik += 5
				get_parent().get_parent().get_node("Positive").play()
			else:
				global.detik = 60
				get_parent().get_parent().get_node("Positive").play()

	get_parent().get_parent().remove_child(self)
	
