extends Label


func _ready():
	if global.score > global.bestscore:
		global.set_bestscore(global.score)
	self.text = "HighScore : " + str(global.bestscore)
