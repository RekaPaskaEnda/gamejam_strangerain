extends LinkButton
export(String) var scene_to_load

func _ready():
	pass # Replace with function body.


func _on_Leaderboard_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
