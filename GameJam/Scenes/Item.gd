extends KinematicBody2D



var multiplier = global.score / 100
var velocity = Vector2()
var number = rand_range(0,7.75)
var coin = preload("res://Assets/kenney_platformerpack/PNG/Items/coinGold.png")
var gem = preload("res://Assets/kenney_platformerpack/PNG/Items/gemRed.png")
var star = preload("res://Assets/kenney_platformerpack/PNG/Items/star.png")
var heart = preload("res://Assets/kenney_platformerpack/PNG/HUD/hudHeart_full.png")
var bad_heart = preload("res://Assets/kenney_platformerpack/PNG/HUD/hudHeart_empty.png")
var fireball = preload("res://Assets/kenney_platformerpack/PNG/Particles/fireball.png")
var saw = preload("res://Assets/kenney_platformerpack/PNG/Enemies/saw.png")
var time_3 = preload("res://Assets/kenney_platformerpack/PNG/HUD/hud3.png")
var time_5 = preload("res://Assets/kenney_platformerpack/PNG/HUD/hud5.png")


onready var sprite = get_node("Sprite")
onready var gravity = multiplier*5 + 10

const UP = Vector2(0,-1)

func _ready():
	print(multiplier)
	if  (number < 1):
		sprite.set_texture(coin)
		sprite.scale.x = 1
		sprite.scale.y = 1
	elif (number < 2):
		sprite.set_texture(gem)
		sprite.scale.x = 0.75
		sprite.scale.y = 1
	elif (number < 3):
		sprite.set_texture(star)
		sprite.scale.x = 1
		sprite.scale.y = 1
	elif (number < 4):
		sprite.set_texture(heart)
		sprite.scale.x = 0.75
		sprite.scale.y = 0.75
	elif (number < 5):
		sprite.set_texture(bad_heart)
		sprite.scale.x = 0.75
		sprite.scale.y = 0.75
	elif (number < 6):
		sprite.set_texture(fireball)
		sprite.scale.x = 1
		sprite.scale.y = 1
	elif (number < 7):
		sprite.set_texture(saw)
		sprite.scale.x = 0.5
		sprite.scale.y = 0.5
	elif (number < 7.5):
		sprite.set_texture(time_3)
	elif (number < 7.75):
		sprite.set_texture(time_5)
	

func _process(delta):
	velocity.y += gravity
	if is_on_floor():
		get_parent().remove_child(self)

func _physics_process(delta):
	velocity.y += delta * gravity
	velocity = move_and_slide(velocity, UP)

func position(delta):
	position.x = delta.x
	position.y = delta.y
