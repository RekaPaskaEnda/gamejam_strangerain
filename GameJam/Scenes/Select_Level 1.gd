extends LinkButton

export(String) var scene_to_load

func _on_Level_1_pressed():
	global.player = 1
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
