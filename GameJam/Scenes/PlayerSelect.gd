extends ColorRect

onready var audio = get_node("AudioStreamPlayer")

func _ready():
	audio.play()
	
func _process(delta):
	audio.stop()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
