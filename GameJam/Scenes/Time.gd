extends Label

export(String) var scene_to_load

# Called when the node enters the scene tree for the first time.
func _process(delta):
	if global.detik == 0:
		get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
	
	if global.detik >= 0:
		self.text = str(global.detik)

func _on_Countdown_timeout():
	global.detik -= 1
