extends LinkButton

export(String) var scene_to_load

func _on_back_button_pressed():
	global.lives = 3
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
