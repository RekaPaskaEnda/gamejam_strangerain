extends Position2D

export (PackedScene) var spawnScene
onready var spawnReference = load (spawnScene.get_path())

export (NodePath) var timePath
onready var timeNode = get_node(timePath)

export (float) var minWaitTime
export (float) var maxWaitTime

func _ready():
	randomize()
	timeNode.set_wait_time(rand_range(minWaitTime, maxWaitTime))
	timeNode.start()


func _on_Time_timeout():
	var spawnInstance = spawnReference.instance()

	get_parent().add_child(spawnInstance)
	spawnInstance.position(global_position)
	
	timeNode.set_wait_time(rand_range(minWaitTime, maxWaitTime))
	timeNode.start()



func _on_Dificulty_time_timeout():
	if minWaitTime > 0.5:
		minWaitTime -= 0.1
	if maxWaitTime > 1:
		maxWaitTime -= 0.1
