extends LinkButton

export(String) var scene_to_load

func _on_LinkButton_pressed():
	global.lives = 3
	global.score = 0
	global.detik = 45
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
