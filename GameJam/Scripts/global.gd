extends Node

var lives = 3
var score = 0
var player = 1
var detik = 45
var bestscore = 0 setget set_bestscore
var satu = 0 setget set_leaderboard
var dua = 0 setget set_leaderboard
var tiga = 0 setget set_leaderboard
var empat = 0 setget set_leaderboard
var lima = 0 setget set_leaderboard

const filepath = "./bestscore.data"

const satu_path = "./1.data"
const dua_path = "./2.data"
const tiga_path = "./3.data"
const empat_path = "./4.data"
const lima_path = "./5.data"

func _ready():
	load_bestscore()
	load_leaderboard()

func set_leaderboard(value):
	var number = 1
	while(number<=5):
		var file = File.new()
		var path = "./"+str(number)+".data"
		file.open(path, File.READ)
		if score > file.get_var():
			store(score, path)
			return
		number += 1

func store(value, path):
	var file = File.new()
	file.open(path,File.WRITE)
	file.store_var(value)
	file.close()

func load_leaderboard():
	var number = 1
	var file = File.new()
	if !file.file_exists(satu_path):
		return
	file.open(satu_path, File.READ)
	satu = file.get_var()
	file.close()
	if !file.file_exists(dua_path):
		return
	file.open(dua_path, File.READ)
	dua = file.get_var()
	file.close()
	if !file.file_exists(tiga_path):
		return
	file.open(tiga_path, File.READ)
	tiga = file.get_var()
	file.close()
	if !file.file_exists(empat_path):
		return
	file.open(empat_path, File.READ)
	empat = file.get_var()
	file.close()
	if !file.file_exists(lima_path):
		return
	file.open(lima_path, File.READ)
	lima = file.get_var()
	file.close()

func load_bestscore():
	var file = File.new()
	if !file.file_exists(filepath):
		return
	file.open(filepath, File.READ)
	bestscore = file.get_var()
	file.close()

func save_bestscore():
	var file = File.new()
	file.open(filepath,File.WRITE)
	file.store_var(bestscore)
	file.close()

func set_bestscore(value):
	bestscore = value
	save_bestscore()

